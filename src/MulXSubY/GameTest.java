package MulXSubY;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class GameTest {
	
	@Test
	public void testCalculate() {
		Game game = new Game(8,11,2,3);
		int x = game.calculate();
		assertEquals(x, 5);
	}

}
