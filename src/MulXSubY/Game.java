package MulXSubY;

import java.util.ArrayList;

/**
 * 
 * @author Stijn
 * 
 * MulXSubY game
 * ONLY WORKS WITH POSITIVE INTEGERS!!!
 *
 */
public class Game {
	
	/**
	 * Main class purpose:
	 * - setup new Game
	 * - call method 'calculate' from class Game
	 */
	public static class Main {
		public static void main(String[] arguments) {
			Game game = new Game(8,11,2,3);
			game.calculate();
		}
	}
	
	//Attributes
	private LinkedBinaryTree<Integer> tree;
	private ArrayList<Integer> list;
	private ArrayList<Position<Integer>> deadleaves;
	private StackAdapter<String> calculations;
	private int endValue, X, Y, counter, calculs;
	private boolean isFound = false;
	
	/**
	 * Constructor Game
	 * @param startValue
	 * @param endValue
	 * @param X is the multiplier
	 * @param Y is the minuend
	 */
	public Game(int startValue, int endValue, int X, int Y) {
		//initialize beginValues
		this.endValue = endValue;
		this.X = X;
		this.Y = Y;
		
		//initialize linked binary tree to store the result of new calculations
		//and initialize 'startValue' as root of 'tree'
		tree = new LinkedBinaryTree<Integer>(startValue);
		//initialize ArrayList to store all different results
		list = new ArrayList<Integer>();
		deadleaves = new ArrayList<Position<Integer>>();
		//initialize Stack to print the calculations in right order
		calculations = new StackAdapter<String>();
	}
	
	/**
	 * Method newLevel executes calculations with the promising values
	 * These values are always leaves from the tree, have not been 
	 */
	public void newLevel() {
		for (Position<Integer> p : tree.positions()){
			
			//check if p is a leaf and if p will have no valuable children
			if (tree.isExternal(p)  && !deadleaves.contains(p)) {	
				//initialize values for parent and 2 children
				int par = p.getElement();
				int mul = par * X;				//rightChild
				int sub = par - Y;				//leftChild
				calculs += 2;
				
				//Check if endValue is found
				if (sub == endValue || mul == endValue){
					isFound = true;		//Breaks while-loop in method calculate
					String s;
					
					/**
					 * Iterates the calculations from endValue to startValue (so backwards)
					 * Therefore Strings are put in stack 'calculations'
					 */
					if(sub == endValue)calculations.push(p.getElement() + " - 3 = " + sub);
					else calculations.push(p.getElement() + " * 2 = " + mul);
					Position<Integer> temp = p;
					while (tree.parent(temp) != null){
						if(tree.parent(temp).getElement() > temp.getElement()) s = " - 3 = ";
						else s = " * 2 = ";
						calculations.push(tree.parent(temp).getElement() + s + temp.getElement());
						temp = tree.parent(temp);
					}
					
					//Break for-loop in this method
					break;
				}
				
				//if endValue not found
				else {
					//check if value is not yet found and if value is higher than zero
					if(!list.contains(sub) && sub > 0)
					{
						tree.addLeft(p, sub);			//add element to tree
						list.add(sub);					//add element to list
					}
					//Check if value is not yet found and if value is not too high
					if(!list.contains(mul) && mul < (X*(Y+endValue)))
					{
						tree.addRight(p, mul);			//add element to tree
						list.add(mul);					//add element to list	
					} 
					//if p has no children --> add p to list 'deadleaves'
					if (tree.isExternal(p)) deadleaves.add(p);
				}
			}
		}
	}
	
	/**
	 * Method for counting the minimum calculations needed to go from the startValue to the endValue
	 * @return number of calculations needed
	 */
	public int calculate() {
		counter = 0;
		isFound = false;
		calculs = 0;
		while(!isFound){
			int elements = tree.size();
			newLevel();									//method to calculate new 'treelevel'
			counter++;
			if (elements == tree.size()) break;			//breaks if no new elements are added to tree
		}
		
		//messages that are printed on screen
		if (!isFound) {
			System.out.println("NOT FOUND");
		}
		else{
			System.out.println("FOUND AFTER " + counter + " CALCULATIONS:");
			//while-loop prints all calculations from stack
			while(!calculations.isEmpty()) {
				System.out.println(calculations.top());
				calculations.pop();
			}
		}
		System.out.println("Total number of calculations: " + calculs);
		System.out.println("Tree contains " + tree.size() + " elements");
		System.out.println("deadLeaves contains " + deadleaves.size() + " elements");
		
		return counter;
	}
}

