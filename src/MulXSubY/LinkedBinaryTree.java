package MulXSubY;


public class LinkedBinaryTree<E> extends AbstractBinaryTree<E> implements Iterable<E>{

	
	/**
	 * Class Node
	 * @param <E>
	 */
	protected static class Node<E> implements Position<E>{
		//Attributes
		private E element;
		private Node<E> parent, left, right;
		
		/**
		 * Constructor for Node
		 * @param e is given element
		 * @param above is parent node
		 * @param leftChild is left node
		 * @param rightChild is right node
		 */
		public Node(E e, Node<E> above, Node<E> leftChild, Node<E> rightChild) {
			element = e;
			parent = above;
			left = leftChild;
			right = rightChild;
		}
		
		/**
		 * returns element of node
		 */
		@Override
		public E getElement() {
			return element;
		}
		
		/**
		 * check if node has children
		 * @return true if node has at least one child
		 */
		public boolean hasChild() {
			return (getLeft() != null) || (getRight() != null);
		}
		
		/**
		 * @return parent of node
		 */
		public Node<E> getParent(){
			return parent;
		}
		
		/**
		 * @return left child of node
		 */
		public Node<E> getLeft(){
			return left;
		}
		
		/**
		 * @return right child of node
		 */
		public Node<E> getRight(){
			return right;
		}
		
		/**
		 * Setter for the element E
		 * @param e
		 */
		public void setElement(E e) {
			element = e;
		}
		
		/**
		 * Setter for parent
		 * @param parentNode
		 */
		public void setParent(Node<E> parentNode) {
			parent = parentNode;
		}
		
		/**
		 * Setter for left child
		 * @param leftChild
		 */
		public void setLeft(Node<E> leftChild) {
			left = leftChild;
		}
		
		/**
		 * Setter for right child
		 * @param rightChild
		 */
		public void setRight(Node<E> rightChild) {
			right = rightChild;
		}
	}
	
	/**
	 * protected Method to create a new node
	 * @param e
	 * @param parent
	 * @param left
	 * @param right
	 * @return
	 */
	protected Node<E> createNode(E e, Node<E> parent, Node<E> left, Node<E> right){
		return new Node<E>(e, parent, left, right);
	}
	
	
	//Attributes
	protected Node<E> root = null;
	private int size = 0;
	
	/**
	 * Constructor for LinkedBinaryTree
	 */
	public LinkedBinaryTree(E e) {
		addRoot(e);
	}
	
	/**
	 * Gives the Node of a given position p
	 * @param p is a position
	 * @return node of given position p
	 * @throws IllegalArgumentException
	 */
	protected Node<E> validate(Position<E> p) throws IllegalArgumentException{
		if(!(p instanceof Node))
			throw new IllegalArgumentException("Not valid position type");
		Node<E> node = (Node<E>) p;
		if(node.getParent() == node)
			throw new IllegalArgumentException("p is no longer in the tree");
		
		return node;
	}
	
	/**
	 * returns size of tree
	 */
	@Override
	public int size() {
		return size;
	}
	
	/**
	 * returns root of tree
	 */
	public Position<E> root(){
		return root;
	}
	
	/**
	 * returns position of parent of given position
	 */
	public Position<E> parent(Position<E> p) throws IllegalArgumentException{
		Node<E> node = validate(p);
		return node.getParent();
	}
	
	/**
	 * returns position of left child of given position
	 */
	public Position<E> left(Position<E> p) throws IllegalArgumentException{
		Node<E> node = validate(p);
		return node.getLeft();
	}
	
	/**
	 * returns position of right child child of given position
	 */
	public Position<E> right(Position<E> p) throws IllegalArgumentException{
		Node<E> node = validate(p);
		return node.getRight();
	}

    /**
     * adds a new node as root of the tree
     * @param e
     * @return
     * @throws IllegalStateException
     */
	public Position<E> addRoot(E e) throws IllegalStateException{
		if(!isEmpty()) throw new IllegalStateException("Tree is not empty");
		root = createNode(e, null,null, null);
		size = 1;
		return root;
 	}
	
	/**
	 * adds a left child to the node
	 * @param p
	 * @param e
	 * @return left child
	 * @throws IllegalArgumentException
	 */
	public Position<E> addLeft(Position<E> p, E e) throws IllegalArgumentException{
		Node<E> parent = validate(p);
		if(parent.getLeft() != null)
			throw new IllegalArgumentException("p already has a left child");
		Node<E> child = createNode(e, parent, null, null);
		parent.setLeft(child);
		size++;
		return child;
	}
	
	/**
	 * adds a right child to the node
	 * @param p
	 * @param e
	 * @return right child
	 * @throws IllegalArgumentException
	 */
	public Position<E> addRight(Position<E> p, E e) throws IllegalArgumentException{
		Node<E> parent = validate(p);
		if(parent.getRight() != null)
			throw new IllegalArgumentException("p already has a right child");
		Node<E> child = createNode(e, parent, null, null);
		parent.setRight(child);
		size++;
		return child;
	}
	
	/**
	 * Sets new element on given position
	 * @param p
	 * @param e 
	 * @return new element
	 * @throws IllegalArgumentException
	 */
	public E set(Position<E> p , E e) throws IllegalArgumentException{
		Node<E> node = validate(p);
		E temp = node.getElement();
		node.setElement(temp);
		return temp;
	}
}
