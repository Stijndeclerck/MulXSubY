package MulXSubY;

import java.util.Iterator;

public interface Tree<E> extends Iterable<E> {
	/**Returns the root of the tree */
	Position<E> root();
	/**Returns the parent of the position */
	Position<E> parent(Position<E> p) throws IllegalArgumentException;
	/**Returns an iterable for the children of the position */
	Iterable<Position<E>> children(Position<E> p) throws IllegalArgumentException;
	/**Returns the number of children of the position */
	int numChildren(Position<E> p) throws IllegalArgumentException;
	/**Returns if a node is internal */
	boolean isInternal(Position<E> p) throws IllegalArgumentException;
	/**Returns if a node is external */
	boolean isExternal(Position<E> p) throws IllegalArgumentException;
	/**Returns if a node the root of the tree */
	boolean isRoot(Position<E> p) throws IllegalArgumentException;
	/**Returns the size of the tree */
	int size();
	/**Returns if the tree is empty */
	boolean isEmpty();
	/**implementation for an iterator for the positions of the tree */
	Iterator<E> iterator();
	Iterable<Position<E>> positions();
}
